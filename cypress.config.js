//const { defineConfig } = require("cypress");
import X2JS from "x2js";
import got from 

module.exports = defineConfig({
  e2e: {
    viewportWidth: 1024,
    viewportHeight: 768,

    hideXHRInCommandLog: true,

    experimentalStudio: true,

    env: {
      username: "kenoid",
      login: "konqer@gmail.com",
      password: "#authorized9",
    },

    async setupNodeEvents(on, config) {
      const sitemapUrl = "https://www.xero.com/sitemap.xml";
      const xml = await got(sitemapUrl).text();
      const x2js = new X2JS();
      const json = x2js.xml2js(xml);
      const urls = json.urlset.url.map((url) => url.loc);
      const trimmedUrls = urls.slice(20, 40);

      config.env.sitemapUrls = trimmedUrls;
      return config;
    },
  },
});
