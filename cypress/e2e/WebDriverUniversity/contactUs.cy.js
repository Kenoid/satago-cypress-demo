import 'cypress-each'
import testdata from "../../fixtures/contactUs.json"

describe('WebDriverUniversity page test suite', { baseUrl: 'http://webdriveruniversity.com' }, () => {

    function getTestTitle(s, k, testdata) {
        return `fills form for "${s.firstName}" - cypress-each: case ${k + 1}/${testdata.length}`
    }

    beforeEach(() => {
        cy.visit('/Contact-Us/contactus.html')
        cy.fixture('contactUs.json').as('contactUs')
    })

    //left just as a reminder how to use aliases in forms
    it('fills empty forms in contact us page - alias', function () {
        cy.get('[name="first_name"]').type(`${this.contactUs[0].firstName}`)
            .get('[name="last_name"]').type(`${this.contactUs[0].lastName}`)
            .get('[name="email"]').type(`${this.contactUs[0].emailAddress}`)
            .get('textarea').type(`${this.contactUs[0].commentBody}`)
            .get(`${this.contactUs[0].button}`).click()

        cy.contains(`${this.contactUs[0].expectedResponse}`)
    })

    //creates one big test that multiplies its steps based on fixture file
    //cons: multiple use of cy.visit + the whole test fails if just one fixture fails
    it('fills form - fixtures array + forEach', () => {
        cy.fixture('contactUs').then((data) => {
            data.forEach((userdata) => {
                cy.visit('/Contact-Us/contactus.html')
                cy.get('[name="first_name"]').type(userdata.firstName)
                    .get('[name="last_name"]').type(userdata.lastName)
                    .get('[name="email"]').type(userdata.emailAddress)
                    .get('textarea').type(userdata.commentBody)
                    .get(userdata.button).click()

                if (userdata.button.includes("submit")) {
                    cy.contains(userdata.expectedResponse, { matchCase: false })
                }
                else {
                    cy.get('#contact_form > *:not(#form_buttons)').should('be.empty')
                }
            })
        })
    })

    //creates a separate test case based on the fixture file
    //pros: readability, cy.visit from beforeEach, unique name for each test, clear separation of failed case
    it.each(testdata)(getTestTitle, (data) => {
        cy.get('[name="first_name"]').type(data.firstName)
            .get('[name="last_name"]').type(data.lastName)
            .get('[name="email"]').type(data.emailAddress)
            .get('textarea').type(data.commentBody)
            .get(data.button).click()

        if (data.button.includes("submit")) {
            cy.contains(data.expectedResponse, { matchCase: false })
        }
        else {
            cy.get('#contact_form > *:not(#form_buttons)').should('be.empty')
        }
    })
})