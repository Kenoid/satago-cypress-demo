describe('WebDriverUniversity homepage test suite', { baseUrl: 'http://webdriveruniversity.com' }, () => {

    beforeEach(() => {
        cy.visit('/')
        cy.fixture('subpages.json').as('subpages')
        cy.fixture('keywords.json').as('keywords')
    })

    it("contains 18 headers", () => {
        cy.get('h1').should('have.length', 18)
    })

    it('checks the presence of all subsections', function () {
        cy.get('h1').each($head => {
            expect($head.text()).to.be.oneOf(this.subpages)
        })
    })

    it('checks the presence of the footer', () => {
        cy.get('footer').should('include.text', 'Copyright © www.GianniBruno.com | Automationteststore')
    })

    it('checks the keywords on the homepage', () => {
        cy.get('@keywords').each($keyword => {
            cy.contains($keyword, { matchCase: false })
        })
    })
})