/// <reference types="cypress" />

describe('Iframe handling with Cypress', { baseUrl: 'https://qavbox.github.io/' }, () => {

    xit('gets the element by chain', () => {
        cy.visit('/demo/iframes/')

        cy.get('#Frame2').its('0.contentDocument.body').find('#frameinput').type('hello world')
    })

    it('gets the element by custom command', () => {
        cy.visit('/demo/iframes/')

        cy.getiFrame('#Frame2').find('#frameinput').type('hello world')
        cy.getiFrameElement('#Frame2', '#frameinput').should('have.value', 'hello world')
    })
})