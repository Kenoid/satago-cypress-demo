//the example shows how to sum up the values from the table using Cypress lodash
//and complare it with the value from the cart summary

describe('Shopping cart suite', { baseUrl: "https://www.demoblaze.com"}, () => {

    const unm = 'konx123'
    const pwd = 'konx123'

    beforeEach(() => {
        cy.loginAndGoToCart(unm, pwd)
    });

    it('sums the values in the cart', () => {
        cy.get('tbody tr', { timeout: 10000 })
          .should('have.length', 3)
          .find('td:nth-child(3)')
          .should('have.length', 3)
          .then(($el) => Cypress._.map($el, 'innerText'))
          .then((list) => list.map(Number))
          .should('have.length', 3)
          .then((list) => {
            let totalValue
    
            cy.get('#totalp').then($el => {
              totalValue = parseInt($el.text())
              expect(Cypress._.sum(list)).to.be.eq(totalValue)
            })
          })
      })
})