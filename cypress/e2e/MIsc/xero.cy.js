import 'cypress-each'

describe('Xero page test suite', { baseUrl: 'https://xero.com' }, () => {

  const urls = Cypress.env('sitemapUrls')

  before(() => {
    expect(urls).to.be.an('array')
  })

  //this test checks if the page doesn't accept manipulation from the automation tool
  it("doesn't let me in from automation tool", () => {
    cy.visit('https://login.xero.com')
    cy.get('#xl-form-email').type(Cypress.env('login'))
    cy.get('#xl-form-password').type(Cypress.env('password'))
    cy.get('#xl-form-submit').click()
    //cy.contains('Access Denied')
    //cy.get('h1', {timeout: 10000}).should('have.text', 'Access Denied')
    cy.get('body').then((body) => console.log(body))

  })

  //this test checks all pages taken from sitemap.xml doc (limited to first 20 for the sake of saving time)
  //and checks if the page is available and contains buttons for signing in and loggin in
  it.each(urls)('checks the page: %s', (url) => {
    cy.visit(url)
    cy.contains('Try Xero for free')
    cy.contains('Log in')
  })
})