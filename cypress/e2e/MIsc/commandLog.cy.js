/// <reference types="Cypress" />

describe('Command log test', { baseUrl: 'https://example.cypress.io' }, () => {

    beforeEach(() => {
        cy.visit('http://webdriveruniversity.com/')

        cy.get('#contact-us > .thumbnail > .caption > p').then((e) => {
            const takeText = e.text()
            const slicedText = takeText.substring(10, 50)
            return slicedText
        }).as('exampleText')
    })

    it('displays cy.log information', () => {
        cy.log('Origin', window.origin)
        cy.visit('/commands/actions')
        cy.log("Cypress info", window.Cypress)

        cy.log(cy.location())
    })

    it('displays Cypress.log() information', () => {
        cy.visit('/commands/storage')
        cy.contains('Populate localStorage').click()
        cy.seedLocalStorage('Las Vegas', 'Raiders').then(() => {
            cy.log(localStorage.getItem('Las Vegas'))
        })
        cy.log(localStorage.getItem('Las Vegas'))
        cy.log(Math.floor(Math.random() * 10))
    })

    it('modifies the displayed info in cy.log for the element', () => {
        //utilize https://www.youtube.com/watch?v=wK_uEcy3sEE
        cy.visit('/')
    })

    it('checks the debugger', () => {
        cy.visit('/cypress-api')
        cy.get('h1').debug().invoke('text').should('contain', 'API')
    })
})