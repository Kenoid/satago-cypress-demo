import newUser from "../../fixtures/test-store-register-input.json";

describe(
  "Login new user",
  { baseUrl: "https://automationteststore.com" },
  () => {
    beforeEach(() => {
      cy.visit("/index.php?rt=account/create");
    });

    it("takes all forms at once", () => {
      cy.get(
        "#AccountFrm div.registerbox input, #AccountFrm div.registerbox select"
      )
        .not('[type="radio"]')
        .each(($el, index) => {
          if ($el.prop("nodeName") == "SELECT") {
            cy.wrap($el).select(newUser[index]);
          } else {
            cy.wrap($el).type(newUser[index]);
          }
        });
    });
  }
);
