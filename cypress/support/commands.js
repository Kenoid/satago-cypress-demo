/// <reference types="Cypress" />

Cypress.Commands.add('seedLocalStorage', (key, value) => {
    Cypress.log({
        name: 'seedLocalStorage',
        displayName: 'seedLS',
        message: `key: ${key}, value: ${value}`,
        consoleProps: () => {
            return {
                'Key': key,
                'Value': value,
                'Local Storage': window.localStorage
            }
        }
    })

    localStorage.setItem(key, value)
})

Cypress.Commands.add('getiFrameElement', function (frameSelector, elementSelector) {
    return cy.get(frameSelector)
        .its('0.contentDocument.body')
        .should('not.be.empty')
        .find(elementSelector)
        .should('exist')
        .then(cy.wrap)
})

Cypress.Commands.add('getiFrame', function (frameSelector) {
    return cy.get(frameSelector)
        .its('0.contentDocument.body').should('not.be.empty')
        .then(cy.wrap)
})

Cypress.Commands.add('loginAndGoToCart', (unm, pwd) => {
    cy.visit('/')
    cy.get('a[data-target="#logInModal"]').click()
    cy.get('#loginusername')
      .should('be.visible')
      .clear()
      .type(unm, { delay: 100 })
      .should('have.value', unm)
    cy.get('#loginpassword')
      .should('be.visible')
      .clear()
      .type(pwd, { delay: 100 })
      .should('have.value', unm)
    cy.contains('button', 'Log in').click()
    cy.contains(unm).should('contain.text', `Welcome ${unm}`)
    cy.visit('/cart.html')
})